package com.randy;

import com.randy.EngineImpl;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class EngineTest   extends TestCase{
	
	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EngineTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( EngineTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    
    public void testEngine()
    {
        Engine engineUT = EngineImpl.getInstance();
        System.out.println("EngineTest.testEngine engineUT.movies:");
        for(Movie m: engineUT.getMovies()) {
        	System.out.println("   movie:"+m.getName()+" "+m.getGenre() );
        }
        
        String id = "5";
        System.out.println("Movie with id:"+id+" is:"+engineUT.findMovieById(id).getName());
        
        for(Cineplex c: engineUT.getCineplexes())
        {
        	System.out.println("   cineplex:"+c.getName()+" id:"+c.getId()+"    " +c.getAddress());
        }
        for(Theater t: engineUT.getTheaters())
        {
        	System.out.println("   theater:"+t.getName()+" id:" +t.getId()+"    cineplexId:"+t.getCineplexId()+"    cineplexName:"+t.getCineplex().getName());  
        }
        
        for(Showing s:engineUT.getShowings()){
        	System.out.println("    showing: id:"+s.getId()+" movie:"+s.getMovie().getName()+"  startDateTime:"+s.getStartDate() +" cineplex:"+s.getTheater().getCineplex().getName()+"  theater:"+s.getTheater().getName());
        }
        assertTrue( true);
    }

}
