package com.randy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.randy.TheaterImpl;

public class EngineImpl implements Engine{

	private Map<String,Movie> movies = new HashMap<String,Movie>();
	private Map<String,Cineplex> cineplexes = new HashMap<String,Cineplex>();
	private Map<String,Theater> theaters = new HashMap<String,Theater>();
	
	private Map<String,Showing> showings = new HashMap<String,Showing>();
	
	private static final Engine theInstance = new EngineImpl();
	
	private EngineImpl() {
		if(theInstance != null){
			throw new IllegalStateException("Already exists");
		}
        	
		ObjectMapper mapper= new ObjectMapper();
		ClassLoader classLoader = getClass().getClassLoader();
		File f = null;
				 
		try {
			 
			f = new File(classLoader.getResource("movies.json").getFile());
			List<MovieImpl> movieListFromFile = Arrays.asList(mapper.readValue(f,MovieImpl[].class));
			for(Movie m: movieListFromFile){
				movies.put(m.getId(),m);  
			}
			
			f = new File(classLoader.getResource("cineplexes.json").getFile());
			List<CineplexImpl> cineplexListFromFile = Arrays.asList(mapper.readValue(f,CineplexImpl[].class));
			for(Cineplex c: cineplexListFromFile){
				cineplexes.put(c.getId(),c);
			}
			 
			f = new File(classLoader.getResource("theaters.json").getFile());
			
			TheaterImpl tix = null;
			List<com.randy.TheaterImpl> theaterListFromFile = Arrays.asList(mapper.readValue(f,TheaterImpl[].class));
			// Hook the theaters up to their cineplexes
			for(TheaterImpl ti: theaterListFromFile){
				ti.setCineplex(this.findCineplexById(ti.getCineplexId()));
			}
			
			
			for(Theater t: theaterListFromFile){
				theaters.put(t.getId(),t);
			}
			
			
			
			f = new File(classLoader.getResource("showings.json").getFile());
			List<ShowingImpl> showingListFromFile = Arrays.asList(mapper.readValue(f,ShowingImpl[].class));
			//hook up movie and theater
			for(ShowingImpl si: showingListFromFile){
				Movie m = movies.get(si.getMovieId());
				Theater t= theaters.get(si.getTheaterId());
				si.setMovie(m);
				si.setTheater(t);
			}
			for(Showing s:showingListFromFile){
				showings.put(s.getId(),s);
			}
			
			
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static Engine getInstance() {
		return theInstance;// == null){
			
		}
	 
	 
	public Movie findMovieById(String id){
		return movies.get(id);
	}
	
	public Collection<Movie> getMovies() {		 
		return movies.values();
	}

	public Cineplex findCineplexById(String id){
		return cineplexes.get(id);
	}
	public Collection<Cineplex> getCineplexes() {
		return cineplexes.values();
	}

 
	public Collection<Theater> getTheaters() {
		return theaters.values();
	}

	public Theater findTheaterById(String id){
		return theaters.get(id);
	}
 
	
	public Collection<Showing> getShowings() {
		return showings.values();
	}

}
