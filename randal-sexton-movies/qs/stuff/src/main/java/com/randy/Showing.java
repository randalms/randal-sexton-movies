package com.randy;

import org.joda.time.DateTime;

public interface Showing {
	public String getId();

	public   Movie getMovie();

	public  Theater getTheater();

	public  DateTime getStartDate();

	public   DateTime getEndDate();

	public  int getSoldSeats();

}