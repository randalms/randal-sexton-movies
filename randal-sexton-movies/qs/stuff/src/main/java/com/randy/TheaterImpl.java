package com.randy;

public class TheaterImpl implements Theater {
	
	private String id;
	private String cineplexId;
	public String getCineplexId() {
		return cineplexId;
	}
	public void setCineplexId(String cineplexId) {
		this.cineplexId = cineplexId;
	}
	private Cineplex cineplex;
	private String name;
	private int seatingCapacity;
	private String theaterType;
	
	
	/* (non-Javadoc)
	 * @see com.randy.Theater#getCineplex()
	 */
	@Override
	public Cineplex getCineplex() {
		return cineplex;
	}
	public void setCineplex(Cineplex cineplex) {
		this.cineplex = cineplex;
	}
	/* (non-Javadoc)
	 * @see com.randy.Theater#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see com.randy.Theater#getSeatingCapacity()
	 */
	@Override
	public int getSeatingCapacity() {
		return seatingCapacity;
	}
	public void setSeatingCapacity(int seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}
	/* (non-Javadoc)
	 * @see com.randy.Theater#getTheaterType()
	 */
	@Override
	public String getTheaterType() {
		return theaterType;
	}
	public void setTheaterType(String theaterType) {
		this.theaterType = theaterType;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
