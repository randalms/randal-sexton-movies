package com.randy;

import java.util.Collection;
import java.util.List;

public interface Engine {
	Collection<Movie> getMovies();
	Collection<Cineplex> getCineplexes();
	Collection<Theater> getTheaters();
	Collection<Showing> getShowings();
	
	Movie findMovieById(String id);
	Cineplex findCineplexById(String id);
    Theater findTheaterById(String id);

}
