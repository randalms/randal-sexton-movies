package com.randy;

public interface Movie {
	String getId();
	String getName();
	String getGenre();

}
