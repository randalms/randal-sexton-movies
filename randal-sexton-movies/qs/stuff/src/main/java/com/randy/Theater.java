package com.randy;

public interface Theater {
	
	public String getId();
	
	public String getCineplexId();

	public Cineplex getCineplex();

	public  String getName();

	public  int getSeatingCapacity();

	public   String getTheaterType();

}