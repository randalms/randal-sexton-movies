package com.randy;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class ShowingImpl implements Showing {
	private String id;
	private String movieId;
	private String theaterId;
	private String startDateS;
	private String endDateS;
	
	private Movie movie;
	private Theater theater;	
	private int soldSeats;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getTheaterId() {
		return theaterId;
	}
	public void setTheaterId(String theaterId) {
		this.theaterId = theaterId;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public Theater getTheater() {
		return theater;
	}
	public void setTheater(Theater theater) {
		this.theater = theater;
	}
	
	
	
	public String getStartDateS() {
		return startDateS;
	}
	
	
	public void setStartDateS(String startDateS) {
		this.startDateS = startDateS;
	}
	
	public String getEndDateS(){
		return endDateS;
	}
	
	public void setEndDateS(String endDateS) {
		this.endDateS = endDateS;
	}
	
	
	
	
	public DateTime getStartDate() {
		DateTimeFormatter f = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		return f.parseDateTime(startDateS);
	}
	
	public DateTime getEndDate() {
		DateTimeFormatter f = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		return f.parseDateTime(endDateS);
		 
	}
	
	public int getSoldSeats() {
		return soldSeats;
	}
	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}
	
	
	
	

}
