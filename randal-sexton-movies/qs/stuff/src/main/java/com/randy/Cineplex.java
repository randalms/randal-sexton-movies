package com.randy;

public interface Cineplex {

	public String getId(); 
	public   String getName();
	public   String getAddress();

}